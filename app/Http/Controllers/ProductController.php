<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

         $myFile = "data.json";
         $jsondata = file_get_contents($myFile);
         $arr_data = json_decode($jsondata, true);
        
        return view('welcome')->with('all',$arr_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         
        
       
         $myFile = "data.json";
         $formdata=[
            'prd'=>$request->input('prd'),
            'qtinstock'=>$request->input('qtinstock'),
            'price'=>$request->input('price'),
            'date'=>date('Y-m-d H:i:s')
         ];
         $jsondata = file_get_contents($myFile);
         $arr_data = json_decode($jsondata, true);
         array_push($arr_data,$formdata);
         $jsondata = json_encode($arr_data, JSON_PRETTY_PRINT);
          if(file_put_contents($myFile, $jsondata)) {
            return view('welcome')->with('all',$arr_data);
            }
         else 
            return "error";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
