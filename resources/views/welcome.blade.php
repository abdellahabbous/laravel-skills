<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        <div class="container">
            <div class="content">
            <div class="col-md-8 col-md-offset-2">
                <form method="post" action="/">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Product name</label>
                        <input type="text"  name="prd" class="form-control" id="exampleInputEmail1" placeholder="Product name">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Quantity in stock</label>
                        <input type="text" name="qtinstock" class="form-control" id="exampleInputPassword1" placeholder="Quantity in stock">
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword1">Price</label>
                        <input type="number" name="price" class="form-control" id="exampleInputPassword1" placeholder="Price">
                      </div>
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <button type="submit" class="btn btn-default">Submit</button>
                    </form>

                    </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Product name</th>
                            <th>Qte</th>
                            <th>Price</th>
                            <th>Total</th>
                            <th>Date</th>
                        </tr>

                    </thead>
                    <tbody>

                        @foreach ((object)$all as $value)
                                <tr>
                                    <td>{{ $value['prd']}}</td>
                                    <td>{{ $value['qtinstock']}}</td>
                                    <td>{{ $value['price']}}</td>
                                    <td>{{ $value['price']*$value['qtinstock']}}</td>
                                    <td>{{ $value['date']}}</td>   
                                </tr>
                                
                             
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </body>
</html>
